﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogController : BaseClass{
	public int tempIndex = 0;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	public void OpenMe(List<string> modelData){
		//if(stateGame == GameState.DIALOG){
			app.dialogView.text.text = modelData[tempIndex];
			app.dialogView.dialogView.SetActive(true);
		//}
	}
	public void CloseMe(){
		//if(stateGame == GameState.IDLE){
			tempIndex = 0;
			app.dialogView.dialogView.SetActive(false);
			app.model.isClicked = false;
		//}
	}
	public void Next(List<string> modelData){
		//Debug.Log(modelData.Count);
		if(tempIndex >= modelData.Count){
			app.model.isClicked = false;
			app.dialogView.dialogView.SetActive(false);
			tempIndex = 0;
		}
		app.dialogView.text.text = modelData[tempIndex];

	}

}
