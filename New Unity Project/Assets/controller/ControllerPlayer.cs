﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControllerPlayer : BaseClass {
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	public void Movement(){
		//Ray ray = Camera.main.ScreenPointToRay(Input.GetTouch(0).position);
		if(Input.GetMouseButtonDown(0)){
			Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
			RaycastHit hit;
			if(Physics.Raycast(ray,out hit)){
				if(hit.transform.gameObject.tag == "Ground"){
					app.playerView.agent.SetDestination(hit.point);
				}
				if(hit.transform.gameObject.tag == "NPC"){
						Debug.Log("OK NPC");
						app.model.isClicked = true;
				}
			}
		}
	}
}
