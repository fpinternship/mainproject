﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ModelPlayer : BaseClass{
	public int Score {get;set;}
	public string Name_Player {get;set;}
	public bool isClicked;
	public ModelPlayer(int Score , string Name_Player){
		this.Score = Score;
		this.Name_Player = Name_Player;
	}
}
