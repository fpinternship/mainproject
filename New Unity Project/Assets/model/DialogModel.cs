﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogModel : BaseClass{
	public List<string> petaniDialog = new List<string>();
	public List<string> travelerDialog = new List<string>();
	public List<string> professorDialog = new List<string>();
	public List<string> prolog = new List<string>();
	public List<string> wargaDialog = new List<string>();
}
