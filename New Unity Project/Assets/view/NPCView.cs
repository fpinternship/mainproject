﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPCView : BaseClass {
	void OnTriggerEnter(Collider other){
		//stateGame = GameState.DIALOG;
		if(other.tag == "Player" && app.model.isClicked){
			Debug.Log("OK");
			app.dialogController.OpenMe(app.dialogModel.petaniDialog);
		}
	}
	void OnTriggerStay(Collider other){
		if(other.tag == "Player" && app.model.isClicked){
			Debug.Log("OK");
			app.dialogController.OpenMe(app.dialogModel.petaniDialog);
		}
	}
	void OnTriggerExit(Collider other){
		//stateGame = GameState.IDLE;
		if(other.tag == "Player"){
			app.dialogController.CloseMe();
		}
	}
}
