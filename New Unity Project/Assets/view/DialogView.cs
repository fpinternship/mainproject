using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class DialogView : BaseClass,IPointerClickHandler {
	public GameObject dialogView;
	public Text text;
	// Use this for initialization
	#region IPointerClickHandler implementation
    public void OnPointerClick(UnityEngine.EventSystems.PointerEventData eventData)
    {
    	Debug.Log("Next");
    	app.dialogController.tempIndex++;
    	app.dialogController.Next(app.dialogModel.petaniDialog);
    }
    #endregion
}
