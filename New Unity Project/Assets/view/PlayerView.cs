using System;
using UnityEngine;
using UnityEngine.AI;
public class PlayerView : BaseClass{
	public NavMeshAgent agent {get;set;}
	void Start(){
		agent = GetComponent<NavMeshAgent>();
	}
	void Update(){
		Moving();
	}
	void Moving(){
		app.playerController.Movement();
	}
}