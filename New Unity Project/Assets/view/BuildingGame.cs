﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BuildingGame : BaseClass {
	void OnTriggerEnter(Collider other){
		if(other.tag =="Player"){
			SceneManager.LoadScene ("MemoryMatch", LoadSceneMode.Additive);
		}
	}
}
