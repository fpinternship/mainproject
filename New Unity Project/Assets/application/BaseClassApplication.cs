﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseClassApplication : MonoBehaviour {
	public ModelPlayer model;
	public BaseView view;
	public ControllerPlayer playerController;
	public PlayerView playerView;
	public DialogModel dialogModel;
	public DialogController dialogController;
	public DialogView dialogView;
	public NPCView npcView;
	public ControllerNPC npcController;
	public ModelNPC npcMOdel;
	public BuildingGame builidingGameView;
	public BuildingGameController miniGameController;
	public BuildingGameModel miniGameModel;
	void Start(){}
}
public class BaseView{

}