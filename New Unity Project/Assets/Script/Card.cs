﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
public class Card : MonoBehaviour,IPointerClickHandler {
	//kondisi card , cek card kondisi buka atau tutup
	public int state;
	//sprite card_back , sprite mengambil dari gamemanager;
	private Sprite card_back;
	//sprite card_front , sprite mengambil dari gamemanager;
	public Sprite card_front;

	// Use this for initialization
	void Start () {
		//kondisi awal , kartu dalam keadaan tertutup
		state = 0;
	}
	public void Flip(){
		//tricky, player membuka kartu , state = 1 dan sebalik nya
		if (state == 0) {
			state = 1;
		} else if (state == 1) {
			state = 0;
		}
		//init sprite mengambil dari singletoon gameManager
		if (state == 0) {
				GetComponent<Image> ().sprite = GameManager.instance.cardBack;
		} else if (state == 1) {
			//card_front == null tetapi sudah di init oleh GameManager;	
			GetComponent<Image> ().sprite = card_front;
		}
	}
	// Update is called once per frame
	void Update () {
	}

	//MouseClickHandler
	#region IPointerDownHandler implementation

	#region IPointerClickHandler implementation


	public void OnPointerClick (PointerEventData eventData)
	{
		Flip ();
		if (state == 1) {
			GameManager.instance.tempCard.Add (this);
			//isSelect = false;
		} else if (state == 0){
			//state = 1;
			GameManager.instance.tempCard.Remove (this);
		}
	}


	#endregion

	//method  ini akan di akses oleh GameManager
	public void isFalse(){
		StartCoroutine (delayFlip());
	}
	IEnumerator delayFlip(){
		yield return new WaitForSeconds (0.3f);
			if (state == 0) {
				GetComponent<Image> ().sprite = GameManager.instance.cardBack;
			GameManager.instance.tempCard.Clear ();
			//state = 1;
			} else if (state == 1) {
				GetComponent<Image> ().sprite = card_front;
			}

	}

	#endregion
}
