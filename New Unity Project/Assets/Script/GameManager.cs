using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;


public class GameManager : MonoBehaviour
{
	public List<Card> tempCard;
	//variable singleToon
	public static GameManager instance;
	//array sprite cardFace sudah di isi oleh inspector
	public Sprite[] cardFace;
	//sprite cardBack sudh di isi oleh inspector
	public Sprite cardBack;
	//array ini menampung gameObject cards
	public GameObject[] cards;
	//tricky
	private int help = 0;

	void Awake()
	{
		//singletoon this
		instance = this;
	}

	void Start()
	{
		//randoming card
		ShuffleCard();
		//Debug.Log (cardFace.Length);
		//passing data dr GameManager ke Card
		//data yang penting adalah CardFace / card_front yang sudah di random
		initCard ();

	}
	void Update()
	{
		Checking ();
	}
	//get-setter
	public Sprite getCardBack ()
	{
		return cardBack;
	}
	//setiap pengulan akan menghasilkan 1-8
	//karena setiap kartu mempunyai pasangan jadi melakuan 2x pengulanan dan akan menghasilkan 1-8 && 1-8
	void initCard ()
	{
		for (int x = 0; x < 2; x++) {
			for (int y = 0; y < cards.Length / 2 ; y++) {
				//cards [rnd].GetComponent<Card> ().cardValue = rnd;
				//cards [rnd].GetComponent<Card> ().Init ();
				//GameObject o = cards[y];
				//Debug.Log(cardFace[y]);
				cards [help].GetComponent<Card> ().card_front = cardFace [y];
				//cards [y].GetComponent<Card> ().Init ();
				++help;
			}
			//random lagi
			ShuffleCard ();
		}
	}
	void ShuffleCard(){
		int n = cards.Length / 2;

		System.Random random = new System.Random ();
		while (n > 1) {
			n--;
			int r = random.Next(n + 1);
			//GameObject val = cards [r];
			//cards [r] = cards [n];
			//cards [n] = val;
			Sprite val = cardFace[r];
			cardFace [r] = cardFace [n];
			cardFace [n] = val;
			//Debug.Log (cardFace [n]);
		}
	}

	void Checking(){
		Debug.Log (tempCard.Count);
		if (tempCard.Count == 2) {
			if (tempCard [0].state == 1 && tempCard [1].state == 1) {
				Matching ();
			}
		}
	}
	void Matching(){
		if (tempCard [0].GetComponent<Image> ().sprite.name == tempCard [1].GetComponent<Image> ().sprite.name) {
			tempCard [0].enabled = false;
			tempCard [1].enabled = false;
			tempCard.Clear ();
			//isSelect = false;
		} else {
			FlipBack ();
		}
	}
	void FlipBack(){
		tempCard [0].GetComponent<Card> ().state = 0;
		tempCard [1].GetComponent<Card> ().state = 0;
		tempCard [1].GetComponent<Card> ().isFalse ();
		tempCard [0].GetComponent<Card> ().isFalse ();
	}

}

