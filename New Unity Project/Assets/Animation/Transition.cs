﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Transition : MonoBehaviour {
	public Vector3 targetPosition = new Vector3(0, -75, 0);
	public float speed = 10.0f;
	public float threshold = 0.5f;
	void Update () {
		threshold = transform.localPosition.y;
		threshold -= speed;
		transform.localPosition = new Vector3(0,threshold,0);
	}
}